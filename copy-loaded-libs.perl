#!/usr/bin/perl

# Takes a destination directory
# and a list of filenames on the command line.
#
# Finds (via "ldd") the shared objects required
# (doesn't work for libraries that get configured
# via /etc, like libnss etc!), and copies these,
# their dependencies, and symlinks that are used
# into the destination directory structure.
#
# If an argument is preceeded by an "-" (minus),
# only copy the dependencies, but not the binary itself --
# that's useful as a shortcut.

my $destination = shift();

our %ignore = ();
our %libs = ();
our @inputs = map { 
	my $ignore = s/^-//;
	print ("IG $_\n"), $ignore{$_}++ if $ignore;
	push @inputs, $_;
	$_;
} @ARGV;


my $child = open(F, "-|");
die "Can't fork" if !defined($child);

if (!$child) {
	exec "ldd", @inputs;
	die "Can't exec ldd";
	exit 1;
}

while (<F>) {
	#print "got $_";
	Add($1), next if (m{^(/\S+):$});

	# /usr/lib/...
	# /lib/...
	# /lib64/...
	while (m{(\S*/lib(64|)/\S*\.so\S*)}g) {
		Add($1);
	}
}


die unless %libs;


my $dest_len = length($destination);
my @libs = sort grep(!$ignore{$_} && (substr($_, 0, $dest_len) ne $destination), keys %libs);

die unless @libs;
print "copy ", join(" ", @libs), "\n";
system("rsync", "-iva", "--stats", "-R", @libs, $destination . "/") and die $!;


sub Add {
	my($lib) = @_;
	my $c = 0;
	while (1) {
		print "adding $lib\n";

		# normalize /a/b/../ to /a/
		1 while $lib =~ s,/+[^/]+/+\.\./+,/,;

		last if $libs{$lib}++;

		last if !-l $lib;
		# retrieve symlink
		
		last if $c++ > 5;

		# symlink
		my $link = readlink($lib) || die $!;
		print "  link $link\n";

		my $base = $lib;
		$base =~ s,[^/]*/*$,,;
		$lib = $link =~ m{^/} ? $link : $base . $link;
	}
}
