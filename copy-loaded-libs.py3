#!/usr/bin/python3
"""Takes a destination directory and a list of filenames on the command line.

Finds (via "ldd") the shared objects required (doesn't work for libraries that get configured
via /etc, like libnss etc!), and copies these, their dependencies, and symlinks that are used
into the destination directory structure. If an argument is preceeded by an "-" (minus),
only copy the dependencies, but not the binary itself -- that's useful as a shortcut.
"""

import sys
import io
import os
import re
import subprocess

# drop command
sys.argv.pop(0)

destination = sys.argv.pop(0)
if not destination:
    os.error("No destination dir")

libs = {}
ignore = {}
inputs = []
for path in sys.argv:
    (p, chg) = re.subn(r'^-', '', path)
    if chg:
        ignore[p] = True
    inputs.append(p)


ldd = subprocess.Popen(['ldd', *inputs],
                       stdin = subprocess.DEVNULL,
                       stdout = subprocess.PIPE,
                       shell = False,
                       #text = True, io.TextIOWrapper wants bytes
                       cwd = '/')


def add_lib(lib):
    """Add library to libs hash."""
    global libs

    c = 10
    while c > 0:
        print("adding %s" % (lib))

		# normalize /a/b/../ to /a/
        did = 1
        while did > 0:
            (lib, did) = re.subn(r'/+[^/]+/+\.\./+', '/', lib, count=1)

        if libs.get(lib):
            break
        libs[lib] = 1
        
        link = None
        try:
            link = os.readlink(lib)
        except OSError as x:
            if x.errno == 22:
                break
            raise os.error("Error with readlink %s" % (lib)) from x

        print("  link %s" % (link))

        if re.search(r'^/', link):
            lib = link
        else:
            base = re.sub(r'[^/]*/*$', '', lib)
            lib = base + link

        c = c - 1
            

for line in io.TextIOWrapper(ldd.stdout, encoding="utf-8"):
    m = re.search(r'^(/\S+):\s*$', line)
    if m:
        add_lib(m.groups(1)[0])
        continue

    deps = re.findall(r'(\S*/lib(?:64|)/\S*\.so\S*)', line)
    for lib_name in deps:
        add_lib(lib_name)

if len(libs.items()) == 0:
    os.error("No libraries found")


print("found %s" % libs)

wanted = [lib_name for (lib_name, val) in libs.items() 
          if not ignore.get(lib_name, 0) and not(lib_name.startswith(destination))]
wanted.sort()
 
if len(wanted) == 0:
    os.error("No libraries required?!")

print("filtered %s\n" % wanted)
sys.stdout.flush()

os.execlp('rsync', 'rsync', '-Riva', '--stats', *wanted, destination + "/")
os.error("can't exec rsync")
