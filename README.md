# Minimization of container images

Given a destination path and a list of binaries and/or shared objects on the command line,
these scripts analyse the dependencies (via `ld`) and copy all required objects to the destination by calling `rsync`.

The scripts are functionally identical - depending on the contents of your build image it might be easier to run the ´perl´ or the `python3` version.

## Example

With a `-` (dash, or minus) prepended *only* the dependencies but not this file itself will be copied.

```
copy-loaded-libs.py3 /image-root/ /bin/bash -/usr/bin/openssl
```

The command above would copy `/bin/bash`, `/lib64/libc.so.6` as a symlink and `/lib/x86_64-linux-gnu/libc.so.6`, as well as `libssl.so` and ´libcrypto.so` and other dependencies but not the `/usr/bin/openssl` binary to the destination directory.

### Example for Java

Here's a much larger example that we used to successfully prune an `ubi-8-openjdk-17` image from 174MB down to 80MB.

```
FROM your-base-image...

RUN mkdir -p /image-root/
COPY my.jar /image-root/

RUN yum install -y java-17-openjdk perl rsync ...

# Version-independent path (via symlinks)
ENV JH=/usr/lib/jvm/jre/

RUN rsync -vaR \
  /usr/lib/locale/C.utf8/ \
  /lib64 \
  /image-root/

RUN env LD_LIBRARY_PATH=${JH}/lib:${JH}/lib/server \
    .../copy-loaded-libs.perl /image-root/ \
  ${JH}/bin/java \
  ${JH}/lib/libmanagement_ext.so ${JH}/lib/libmanagement.so \
  ${JH}/lib/jvm.cfg \
  ${JH}/conf/security/java.security \
  ${JH}/lib/security/default.policy \
  ${JH}/lib/modules \
  ${JH}/lib/tzdb.dat \
  ${JH}/lib/libnio.so ${JH}/lib/libsystemconf.so \
  ${JH}/lib/libzip.so ${JH}/lib/server/*.so \
  ${JH}/lib/libextnet.so ${JH}/lib/libjli.so \
  ${JH}/lib/libjimage.so \
  /usr/lib64/libnss3.so /usr/lib64/libnss_files-2.28.so


# Last stage, actual image
FROM scratch

COPY --from=0 /image-root/ /
CMD ["${JH}/java", ..., "-jar", "/my.jar"]
```


## Requirements

- `perl` (on RHEL8-compatible systems (like `ubi8`) already installed via `git`)
- or `python3` (installed in base `ubi8` by default)
- `rsync` (to copy files and symlinks as a tree structure) (could be replaced by `tar`, or the scripts could copy themselves -- but `rsync` won't be in the destination image anyway...)
